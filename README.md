Make ascii plots in your terminal by combining gnuplot with subprocess

Easy and quick way to plot loss functions and precision/recall curves.

Don't waste time with matplotlib, or waiting for these simple diagnostics to
load through X11 forwarding.

The bottom of asciiplot.py has simple
