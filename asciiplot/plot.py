"""
Written by user: samhaug
2022 Mar 30 01:12:56 PM
"""
import subprocess as sp
import logging
import numpy as np


LOG = logging.getLogger(__name__)

def _run_gnuplot_commands(commands):
    '''
    opens subprocess and executes gnuplot commands

    '''
    try:
        proc = sp.Popen(['gnuplot'],
                        stdout=sp.PIPE,
                        stdin=sp.PIPE,
                        stderr=sp.PIPE)

    except FileNotFoundError:
        LOG.warning('Cannot locate gnuplot executable')
        return ''

    out, err = proc.communicate(input='\n'.join(commands).encode())
    proc.terminate()
    out, err = out.decode(), err.decode()
    if out.startswith('\f'):
        out = out[1:]
    if err:
        LOG.warning(err)

    return out

def precision_recall_curve(prec, recl, size=(79,30), title=''):
    '''
    Returns ASCII precision/recall curve
    '''

    prec = np.asarray(prec, dtype=float)
    recl = np.sort(np.asarray(recl, dtype=float))

    if prec.size == 0:
        LOG.info("No data to plot")
        return ''

    commands = ['set term dumb size %s,%s'%size,
                'set title %r'%title,
                'set xlabel "r"',
                'set ylabel "p"',
                'set xrange [0:1]',
                'set yrange [0:1]',
                'set xtics 0, 0.2, 1',
                'set ytics 0, 0.2, 1',
                'set tics nomirror',
                'set key off',
                'set grid',
                "plot '-' with filledcurves above y=0"]
    commands += ['%f %f'%(r,p) for p,r, in zip(prec, recl)]
    commands += ['e']
    out = _run_gnuplot_commands(commands)
    return out

def xy(x, 
       y, 
       log_y=False, 
       log_x=False, 
       size=(99,32), 
       title=''):
    '''
    Returns simple x/y plot

    x: Numpy shape (N,)

    y: Numpy array or list of arrays up to length 4 of shape (N, )

    log_y. log_x: Booleans to toggle log scale on each axis

    size: (79, 30) default. Tuple to control plot size

    title: optional title of plot
    '''

    commands = ['set term dumb size %s,%s' % size,
                'set title "%s"'%title,
                'set tics nomirror',
                'set tics scale 0.0',
                'set key off',
                'set grid']

    if log_x:
        commands += ['set logscale x']
    if log_y:
        commands += ['set logscale y']

    # If y is a numpy array
    if isinstance(y, np.ndarray) and len(y.shape) == 1:
        commands += ["plot '-' with line ls 2"]
        commands += ['%f %f'% (xd, yd) for xd, yd in zip(x, y)]
        commands += ['e']

    elif isinstance(y, list):
        if len(y) == 2:
            commands += ["plot '-' with line ls 2, '-' with line ls 3"]
        elif len(y) == 3:
            commands += ["plot '-' with line ls 2, '-' with line ls 3, '-' with line ls 4"]
        elif len(y) == 4:
            commands += ["plot '-' with line ls 2, '-' with line ls 3, '-' with line ls 4, '-' with line ls 5"]
        elif len(y) == 5:
            commands += ["plot '-' with line ls 2, '-' with line ls 3,"+
                         " '-' with line ls 4, '-' with line ls 5, '-' with line ls 6"]
        else:
            LOG.info("Can plot up to 5 arrays")
            raise NotImplementedError

        for i in (y):
         commands += ['%f %f'% (x,y) for x, y in zip(x, i)]
         commands += ['e']

    out = _run_gnuplot_commands(commands)
    return out

def loss(iters, 
         loss, 
         test_loss=None,
         loss_bounds=None, 
         log_x=False, 
         log_y=False, 
         size=(79,30), 
         title='loss'):
    '''
    Returns ASCII plot showing loss function in log-scale

    iters: list or numpy array of epochs. 1D (E, )

    loss: numpy array size (E,) or list of numpy arrays of train/validation

    test_loss: (None default) or add (E,) array for a second loss function

    loss_bounds: Tup 

    log_x/log_y: (boolean) True or false for log scales

    size: (79, 30) default. Tuple to control plot size

    title: string
    '''

    iters = np.asarray(iters, dtype=int)
    loss = np.array(loss, dtype=float)
    if loss_bounds is not None:
        loss_bounds = np.array(loss_bounds, dtype=float)

    if iters.size == 0:
        LOG.info('No data to plot')
        return ''

    xy(iters, 

    return out

def hist(x, 
         bins=15,
         size=(79,30), 
         grid=False):

    x = np.array(x).ravel()
    num, bins= np.histogram(x, bins=bins)

    commands = ['set term dumb size %s,%s' % size,
                'set style data histograms',
                'set title "mean: %5.2f std: %5.2f"'%(np.mean(x), np.std(x)),
                'set tics nomirror',
                'set tics scale 0.0',
                'set key off']
    if grid:
        commands += ['set grid']

    commands += ["plot '-' using 2:xtic(1)"]
    commands += ['%.1f %8d'% (x,y) for x, y in zip(bins, num)]
    commands += ['e']
    out = _run_gnuplot_commands(commands)

    return out


if __name__ == '__main__':
#    TEST PRECISION/RECALL CURVE 
#    prec = [0.8, 0.9, 0.95, 0.7, 0.6, 0.9, 0.95, 0.5]
#    recl = np.linspace(0,0.8,num=len(prec))
#    out = precision_recall_curve(prec, recl)
#    print(out)

#    TEST LOSS CURVE 
    iters = np.linspace(0, 500)
    l = -3*iters + 0
    loss_bounds = np.vstack((l + 500, l -500))
    out = loss(iters, l, loss_bounds=loss_bounds, log_y=False)
#    out = loss(iters, l)
    print(out)

#    TEST HISTORGRAM  
#     y = np.random.randn(1000)
#     out = hist(y, bins=60)
#     print(out)


#    TEST XY
#    x = np.linspace(-3, 3)
#    y1 = x**2
#    y2 = 2+x**2
#    y3 = 3*x
#    y4 = -3*x
#    y5 = 1*x
#    print(xy(x, np.sin(3*x), title='Test plot'))
#    print(xy(x, [y1, y2, y3, y4, y5], title='Test plot'))









