"""
Written by user: samhaug
2022 Mar 30 02:15:35 PM
"""
from distutils.core import setup

setup(name='asciiplot',
      version='1.0',
      description='plot in console with ascii',
      author='Samuel Haugland',
      author_email='sam@neurelia.net',
      packages=['asciiplot']
     )
